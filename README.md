A union of philanthropists is collecting Port wine. To describe it, the administrator of the union created following class: Port (see attachment port.h).
Function-member Show() prints following info:

Brand: Gallo
Kind: tawny
Bottles: 20

Function operator<<() prints following info (without end of line in the end):
Gallo, tawny, 20

Having finished definition of class Port, the administrator created a subclass VintagePort before he was fired (please see attachment vintage_port.h).

You now have to finish his work defining both classes:
1) Recreate Port class definition, as the original file was destroyed
2) Try to explain why some function members were overridden and some - were not
3) Try to explain why functions operator=() and operator<<() are not defined as virtual
4) Create a definitions for VintagePort class
5) Fix errors in the original files (if there are any)
