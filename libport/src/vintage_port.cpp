#include "vintage_port.h"
#include <cstring>
#include <iostream>

VintagePort::VintagePort() : Port("none", "vintage") {
    nickname = new char[strlen(NONE) + 1];
    strcpy(nickname, NONE);
    year = 0;
}

VintagePort::VintagePort(const char* br, int b, const char* nn, int y) : Port(br, "vintage", b) {
    nickname = new char[strlen(nn) + 1];
    strcpy(nickname, nn);
    year = y;
}

void VintagePort::Show() const {
    Port::Show();
    std::cout << "Nickname:" << nickname << std::endl;
    std::cout << "Year:" << year << std::endl;
}
