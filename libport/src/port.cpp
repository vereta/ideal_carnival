#include <cstring>
#include "port.h"

Port::Port(const char* br, const char* st, int b) {
    brand = new char[strlen(br) + 1];
    strcpy(brand, br);

    strncpy(style, st, sizeof(style));

    bottles = b;
}

Port::Port(const Port& p) {
    brand = new char[strlen(p.brand) + 1];
    strcpy(brand, p.brand);

    strncpy(style, p.style, sizeof(style));

    bottles = p.bottles;
}

bool operator==(const Port &lhs, const Port &rhs) {
    return (!strcmp(lhs.brand, rhs.brand)) &&
            (!strcmp(lhs.style, rhs.style)) &&
            lhs.bottles == rhs.bottles;
}

Port& Port::operator+= (int b) {
    bottles += b;

    return *this;
}

Port& Port::operator-= (int b) {
    bottles -= b;

    return *this;
}

Port& Port::operator= (const Port & p) {
    delete [] brand;

    brand = new char(strlen(p.brand) + 1);
    strcpy(brand, p.brand);
    strncpy(style,p.style, strlen(p.style));
    bottles = p.bottles;

    return *this;
}

void Port::Show() const {
    std::cout << "Brand:" << brand << std::endl;
    std::cout << "Kind:" << style << std::endl;
    std::cout << "Bottles:" << bottles << std::endl;
}

std::ostream& operator<< (std::ostream& os, const Port& p) {
    return os << p.brand << ", " << p.style << ", " << p.bottles;
}
