#include <gtest/gtest.h>
#include <sstream>
#include "port.h"
#include "vintage_port.h"

TEST(test_Port, test_constructor_empty)
{
    Port p;

    EXPECT_EQ(p.BottleCount(), 0);
}

TEST(test_Port, test_constructor_param)
{
    Port p("test1","test2", 42);

    EXPECT_EQ(p.BottleCount(), 42);
}

TEST(test_Port, test_constructor_default_param)
{
    Port p("test1","test2");

    EXPECT_EQ(p.BottleCount(), 0);
}

TEST(test_Port, test_constructor_copy)
{
    Port p1("test1","test2", 42);
    Port p2(p1);


    EXPECT_TRUE(p1 == p2);
}

TEST(test_Port, test_show)
{
    Port p;

    p.Show();
}

TEST(test_Port, test_ostream)
{
    Port p("test1","test2", 42);
    std::stringstream ss;

    ss << p;

    ASSERT_STREQ(ss.str().c_str(), "test1, test2, 42");
}

TEST(test_Port, test_operator_eq)
{
    Port p1("test1","test2", 42);
    Port p2;

    p2 = p1;

    EXPECT_TRUE(p1 == p2);
}

TEST(test_Port, test_operator_eqeq1)
{
    Port p1("test1","test2", 42);
    Port p2("test1","test2", 42);

    EXPECT_TRUE(p1 == p2);
}

TEST(test_Port, test_operator_eqeq2)
{
    Port p1("test1","test2", 0);
    Port p2("test1","test2", 42);

    EXPECT_FALSE(p1 == p2);
}

TEST(test_Port, test_operator_eqeq3)
{
    Port p1("test1","test42", 42);
    Port p2("test1","test2", 42);

    EXPECT_FALSE(p1 == p2);
}

TEST(test_Port, test_operator_eqeq4)
{
    Port p1("test11","test2", 42);
    Port p2("test1","test2", 42);

    EXPECT_FALSE(p1 == p2);
}

TEST(test_Port, test_plus_eq)
{
    Port p("test1","test2", 42);

    p += 1;

    EXPECT_EQ(p.BottleCount(), 43);
}

TEST(test_Port, test_minus_eq)
{
    Port p("test1","test2", 42);

    p -= 1;

    EXPECT_EQ(p.BottleCount(), 41);
}

TEST(test_VintagePort, test_constructor_empty)
{
    VintagePort p;
}

TEST(test_VintagePort, test_constructor_param)
{
    VintagePort p("brand", 42, "Old Velvet", 1965);
}

TEST(test_VintagePort, test_show)
{
    VintagePort p;

    p.Show();
}
